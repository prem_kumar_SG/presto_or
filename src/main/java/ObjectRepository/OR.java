package ObjectRepository;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import java.util.*;

public class OR {
	
	 @FindBy(id="com.puc.presto:id/login_mobile_met")
	 public static WebElement txtUSername;
	 
	 @FindBy(id="com.puc.presto:id/login_next_btn")
	 public static WebElement btnNext;
	 
	 @FindBys(@FindBy(id="com.puc.presto:id/login_next_btn"))
	 public static List<WebElement> lstNext;
	 
	 
	 @FindBy(id="com.puc.presto:id/login_pwd_met")
	 public static WebElement txtPassword;
	 
	 @FindBy(xpath="//android.widget.TextView[@text='Transfer']")
	 public static WebElement lnkTransfer;
	 
	 @FindBy(id="com.puc.presto:id/transfer_amount_edt")
	 public static WebElement txtAmount;
	 
	 @FindBy(id="com.puc.presto:id/transfer_note_met")
	 public static WebElement txtNote;
	 
	 @FindBy(id="com.puc.presto:id/transfer_passcode_met")
	 public static WebElement txtPasscode;
	 
	 @FindBy(id="com.puc.presto:id/transfer_share_btn")
	 public static WebElement btnShare;
	
	 @FindBy(id="com.puc.presto:id/sms_code1_tv")
	 public static WebElement txtTransactionPin_1;
	 
	 @FindBy(id="com.puc.presto:id/sms_code2_tv")
	 public static WebElement txtTransactionPin_2;
	 
	 @FindBy(id="com.puc.presto:id/sms_code3_tv")
	 public static WebElement txtTransactionPin_3;
	 
	 @FindBy(id="com.puc.presto:id/sms_code4_tv")
	 public static WebElement txtTransactionPin_4;
	 
	 @FindBy(id="com.puc.presto:id/sms_code5_tv")
	 public static WebElement txtTransactionPin_5;
	 
	 @FindBy(id="com.puc.presto:id/sms_code6_tv")
	 public static WebElement txtTransactionPin_6;
	 
	 @FindBy(id="com.puc.presto:id/select_friend_search_met")
	 public static WebElement SelectSrchFrnd;
	 
	 @FindBy(id="com.puc.presto:id/friend_search_met")
	 public static WebElement txtSrchFrnd;
	 
	 @FindBy(xpath="//android.widget.LinearLayout[@index='0']")
	 public static WebElement lblFrnd;
	 
	 @FindBy(id="com.puc.presto:id/transfer_with_user_transfer_btn")
	 public static WebElement btnTransfer;
	 
	 @FindBy(id="com.puc.presto:id/transfer_completed_done_btn")
	 public static WebElement btnDone;
	
}
